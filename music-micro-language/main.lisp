(ql:quickload '(:assets :clm) :silent t)
(unless (uiop:file-exists-p "ins/v.fasl")
  (in-package :clm)
  (cl:compile-file "ins/v.ins")
  (in-package :cl-user))
(load "ins/v")

(defpackage music-micro-language
  (:nicknames :mml)
  (:use :common-lisp)
  (:import-from :clm
                #:with-sound
                #:fm-violin)
  (:import-from :assets))
(in-package :mml)


(defun key-num (name)
  (let* ((key-name (symbol-name name))
         (num (position (char key-name 0) "A BC D EF G")))
    (if (null num)
        nil
        (+ num
           (if (= (length key-name) 1)
               0
               (ecase (char key-name 1)
                 (#\+ 1)
                 (#\- -1)))))))

(defun key->freq (n o)
  (let ((num (key-num n)))
    (unless (null num)
      (let* ((pitch-ratio (/ num 12))
             (relative-octave (- o 4)))
        (when (find (char (symbol-name n) 0) "AB")
          (incf relative-octave))
        (* 440 (expt 2 (+ pitch-ratio relative-octave)))))))

(defun n->rat (n)
  (* (/ 1 32) (expt 2 n)))

(defun lens->pos (tempo lenlis)
  (let ((sec 0)
        (measure (/ tempo 60 4)))
    (loop :for len :in lenlis
          :for len-sec := (float (* measure (n->rat len)))
          :collect (list sec len-sec) :do (incf sec len-sec))))

(defun compile-mml (tempo mml)
  (flet ((->keys (k) (apply #'key->freq (reverse (rest (reverse k))))))
    (let ((keys (mapcar #'->keys mml))
          (pos (mapcar #'third mml)))
      (mapcar #'cons keys (lens->pos tempo pos)))))

(defun build-note (note)
  (unless (null (first note))
    `(fm-violin ,@(rest note) ,(first note) 0.2)))

(defmacro with-mml ((tempo) &body mmls)
  `(with-sound ()
     ,@(loop :for mml :in mmls
             :append (mapcar #'build-note (compile-mml tempo mml)))))
