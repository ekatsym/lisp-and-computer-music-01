/* translate filter-white-noise in /home/ekatsym/training/etc/Lisp-and-Computer-Music-01/playground/ins/ugex.ins to C
 *   written Sat 18-Apr-20 at 18:49 by CLM of 8-May-18
 */

#include <mus-config.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <math.h>
#include <signal.h>
#include <cmus.h>

static sig_atomic_t got_sigint = 0; /* catch C-C if hung */
static void sig_err(int sig) {got_sigint = sig;}

int clm_filter_white_noise (double *clm_double, int datar_len, int *clm_int, int datai_len)
{
  /* The _clm_* variables are temporary values generated by the run macro */
  void (*old_SIGINT)(int);
  mus_long_t I;
  double _clm_6, _clm_5, _clm_4, _clm_3;
  mus_any *_OUTPUT_ = NULL, *NOI = NULL, *F = NULL;
  void *all_gens = NULL;
  mus_long_t clm_beg, clm_end;
  
  all_gens = clm_make_genbag();
  clm_beg = clm_to_mus_long_t(clm_int, 1);
  clm_end = clm_to_mus_long_t(clm_int, 3);
  I = clm_int[17];
  #define AMP 7
  #define AMP_r 1
  #define G 9
  #define G_r 2
  if (CLM_FORMANT_P(11))
    F = clm_add_gen_to_genbag(all_gens, 
      mus_make_formant(
        clm_double[CLM_RLOC(11) + 1],
        clm_double[CLM_RLOC(11)]));
  if (CLM_RAND_P(13))
    NOI = clm_add_gen_to_genbag(all_gens, 
      mus_make_rand_with_distribution(
        clm_double[CLM_RLOC(13)],
        clm_double[CLM_RLOC(13) + 1],
        (clm_int[CLM_ILOC(13) + 2] > 0) ? ((double *)(clm_double + CLM_RLOC(13) + 2)) : NULL,
        clm_int[CLM_ILOC(13) + 2]));
  _OUTPUT_ = clm_output();
  if (clm_beg > clm_end) return(1);
  got_sigint = 0; old_SIGINT = clm_signal(SIGINT, sig_err);                     /* trap SIGINT */
  I = clm_beg;                                                                  /* pass counter */

SAMPLE_LOOP_BEGIN:
  if (got_sigint != 0) {clm_int[0] = (int)got_sigint; goto RUN_ALL_DONE;}
  if (I > clm_end) {I = clm_end; goto RUN_ALL_DONE;}
                                                                                /* (progn */
  _clm_6 = mus_rand(NOI, 0.0);
  _clm_5 = mus_formant(F, _clm_6);                                              /* (formant F _clm_6) */
                                                                                /* (* amp g _clm_5) */
  _clm_4 = ((((clm_int[AMP] == 2) ? clm_double[AMP_r] : (double)(clm_int[AMP + 1]))) * (((clm_int[G] == 2) ? clm_double[G_r] : (double)(clm_int[G + 1]))) * (_clm_5));
  _clm_3 = mus_out_any(I, _clm_4, 0, _OUTPUT_);
  I++;                                                                          /* increment pass counter and loop */
goto SAMPLE_LOOP_BEGIN;

RUN_ALL_DONE:
  clm_signal(SIGINT,old_SIGINT);
  if (all_gens) clm_free_genbag(all_gens);

  return(1);
}

