(ql:quickload '(:clm :assets) :silent t)

(defpackage lisp-and-computer-music-01-playground
  (:nicknames :lcm1.pg)
  (:use :cl :clm)
  (:import-from :assets
                #:enum))
(in-package :lcm1.pg)


;;; violin test
(compile-file "ins/v.ins")
(load "ins/v")

(with-sound ()
  (fm-violin 0 0.5 880 1))


;;; ugex test
(compile-file "ins/ugex.ins")
(load "ins/ugex")

(with-sound ()
  (touch-tone-telephone (enum 0 9)))


;;; key->freq
(defun key-num (name)
  (let* ((key-name (symbol-name name))
         (num (position (char key-name 0) "A BC D EF G")))
    (if (null num)
        nil
        (+ num
           (if (= (length key-name) 1)
               0
               (ecase (char key-name 1)
                 ((#\+) 1)
                 ((#\-) -1)))))))

(defun key->freq (n o)
  (let ((num (key-num n)))
    (unless (null num)
      (let* ((pitch-ratio (/ num 12))
             (relative-octave (- o 4)))
        (when (find (char (symbol-name n) 0) "AB")
          (incf relative-octave))
        (* 440
           (expt 2 (+ pitch-ratio relative-octave)))))))

(key->freq :a 4)
(key->freq :d+ 3)
(key->freq :r 4)

(with-sound ()
  (fm-violin 0   0.5 (key->freq :c 3) 1)
  (fm-violin 0.5 0.5 (key->freq :d 3) 1)
  (fm-violin 1   0.5 (key->freq :e 3) 1)
  (fm-violin 1.5 0.5 (key->freq :f 3) 1)
  (fm-violin 2   0.5 (key->freq :g 3) 1)
  (fm-violin 2.5 0.5 (key->freq :a 3) 1)
  (fm-violin 3   0.5 (key->freq :b 3) 1)
  (fm-violin 3.5 0.5 (key->freq :c 4) 1))

(defun n->rat (n)
  (* 1/32 (expt 2 n)))

(defun lens->pos (tempo lenlis)
  (let ((sec 0)
        (measure (/ tempo 60 4)))
    (loop :for len :in lenlis
          :for len-sec := (float (* measure (n->rat len)))
          :collect (list sec len-sec)
          :do (incf sec len-sec))))

(lens->pos 120 '(4 4 3 4 3))
(lens->pos 60 '(4 4 2 2))


;;; Music Micro Language
(defun compile-mml (tempo mml)
  (flet ((->keys (k)
           (apply #'key->freq (reverse (cdr (reverse k))))))
    (let ((keys (mapcar #'->keys mml))
          (pos (mapcar #'third mml)))
      (mapcar #'cons keys (lens->pos tempo pos)))))

(defun build-note (note)
  (unless (null (first note))
    `(fm-violin ,@(rest note) ,(first note) 0.2)))

(defmacro with-mml ((tempo) &body mmls)
  `(with-sound ()
     ,@(loop :for mml :in mmls
             :append (mapcar #'build-note (compile-mml tempo mml)))))

(with-mml (120)
  ((e 4 3) (b 3 3) (e 4 3) (g- 4 3) (r 4 3) (b 4 3) (r 4 3)
   (g- 4 3) (r 4 3) (e 4 3) (r 4 3) (b 3 3)
   (e 4 3) (b 3 3) (e 4 3) (g- 4 3)))


;;; hello sin
(compile-file "ins/my-sin.ins")
(load "ins/my-sin")

(with-sound ()
  (my-sin 0 1 440 0.3))


